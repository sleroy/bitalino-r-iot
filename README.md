> - **Prix public : 99€**
> - Les membres du [Forum Premium](http://forumnet.ircam.fr/fr/produit/offre-premium-individuel/) bénéficient de  25% de réduction sur le prix public. [Demandez le code de réduction](http://forumnet.ircam.fr/shop/en/contact-us)
> 
> - [Site bitalino](https://bitalino.com/index.php/en/r-iot-kit)

Conçu initialement par l’IRCAM (équipe [ISMM](http://ismm.ircam.fr)), une version DIY et hacker est née de notre fructueuse collaboration avec notre partenaire pluX: le [BITalino R-IoT](https://bitalino.com/en/r-iot-kit) est destiné à servir vos besoins en matière de capteur inertiel sans fil avec un capteur 9 axes dernière génération comportant  un accéléromètre 3 axes, un gyroscope 3 axes et un magnétomètre 3 axes, le tout en résolution 16 bits.

![](https://forum.ircam.fr/media/uploads/bitalino-r-iot-pcb-front.jpg)
![](https://forum.ircam.fr/media/uploads/bitalino-r-iot-pcb-back.jpg)

Le [BITalino R-IoT](https://bitalino.com/en/r-iot-kit) est un capteur inertiel haute résolution de la taille d’un timbre poste avec des capacité d’analyse du mouvement et de streaming des capteurs via WiFi et Open Sound Control (OSC).

Le BITalino R-IoT est basé sur le puissant microcontrôleur Texas Instruments CC3200, un ARM Cortex 32 bits cadencé à 80 MHz remplis de ressources combiné à une programmation de type Arduino à travers l’environnement de développement [Energia IDE](http://energia.nu). Le coeur ARM exécute le programme (personnalisable) et gère la pile Ethernet et WiFi. Il est également compatible avec les outils de développement Texas Instruments comme Code Composer. Energia est quant à lui un portage de l’environnement de programmation [Arduino](https://www.arduino.cc) pour les processeurs TI.

## Application principales

Capture du mouvement sans fil, analyse, transmission de capteurs sans fil, création d’interfaces gestuelles sur mesure, transmetteur WiFi pour écosystème BITalino vers le logiciel [OpenSignal](https://bitalino.com/en/software).
Utilisable avec n’importe quelle plateforme logicielle compatible OSC, comme Max/MSP (exemples disponibles dans la distribution  [MuBu](https://forum.ircam.fr/projects/detail/mubu/) pour Max).

## Caractéristiques techniques
Dimensions : 20.5 x 34 x 5 mm (5 g + le poids de la batterie)

Alimentation : 3.3 à 4.6V – 90mA @5ms (WiFi allumé)

Portée : 50 m avec un routeur 2.4 GHz en ligne de mire

Autonomie : > 4 heures avec une batterie li-polymère de 500mAh (peut varier en fonction des autres équipements connectés)

## Configuration requise
Un routeur / point d’accès WiFi 2.4 GHz

Un ordinateur et un logiciel ou un environnement de développement compatible avec Open Sound Control (Max/MSP ou Pure Data par exemple)

Un port USB et les drivers de Port Virtuel pour câble USB série (Virtual Com Port [FTDI](https://www.ftdichip.com/Drivers/VCP.htm) driver) pour la configuration et programmation via le port série, ainsi que pour  l’utilisation en mode console

Un port Ethernet sur l’ordinateur utilisés. Bien qu’il soit possible de connecter l’ordinateur également via WiFi au routeur / point d’accès, cela dégrade la bande passante disponible et nous recommandons l’utilisation d’une connexion filaire entre le routeur et l’ordinateur (un adaptateur Ethernet / Thunderbolt peut être nécessaire sur certains ordinateurs récents comme les portables MacBook).

## Conception et Développement
Le module R-IoT module a été conçu et développé par [Emmanuel FLETY](https://www.ircam.fr/person/emmanuel-flety) au sein des équipes IRCAM [ISMM](http://ismm.ircam.fr) et PIP dans le cadre des projets [MusicBricks](https://musictechfest.net/musicbricks/) et [RapidMix](http://rapidmix.goldsmithsdigital.com). La version BITalino R-IoT du module a été conçue sous la supervision d’Emmanuel FLETY (IRCAM) et de Rui FREIXO et Hugo SILVA (pluX).

> - [Support technique](https://bitalino.com/en/support/contactus)
> - [FAQ](https://bitalino.com/en/support/faq)